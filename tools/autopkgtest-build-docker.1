.TH autopkgtest\-build\-docker 1 2018 "Linux Programmer's Manual"
.SH NAME
autopkgtest\-build\-docker, autopkgtest\-build\-podman \- Create or update autopkgtest image for autopkgtest\-virt\-docker, autopkgtest\-virt\-podman

.SH SYNOPSIS
.B autopkgtest\-build\-docker
.RI [ options ]

.B autopkgtest\-build\-podman
.RI [ options ]

.SH DESCRIPTION
.B autopkgtest\-build\-docker
creates or updates a Docker or Podman image which is suitable for autopkgtest's
Docker/Podman runner
.BR autopkgtest\-virt\-docker (1)
or
.BR autopkgtest\-virt\-podman (1).

When run as
.B autopkgtest\-virt\-podman
or with the
.B \-\-podman
option (recommended), Podman is used for building images.
This can be done unprivileged.

When run as
.B autopkgtest\-virt\-docker
or with the
.B \-\-docker
option, Docker is used.
This requires root-equivalent privileges on the host system.
Using Podman instead of Docker is recommended.

The images produced by this tool are intended to be suitable for use with
either Podman or Docker.

The image used for testing is built using
.BR docker\-build (1)
or
.BR podman\-build (1)
from a base image or tarball,
using a recipe that adds
.B deb\-src
apt sources, runs
.B apt\-get update\fR
and installs build dependencies.

If the
.B \-\-tarball
option is used,
the image used for testing is based on that tarball,
and no base image is downloaded.

Otherwise,
it is based on a pre-existing base image.
The default is to download a suitable base image for the detected or
configured OS vendor from a third-party container registry.
The
.B \-\-image
option can be used to select a different base image.

You can specify an apt proxy to use in the container in the
.B \-\-apt\-proxy
parameter. If you have an apt proxy configured on the host,
the container will automatically use this, otherwise there is no default.


.SH OPTIONS

.TP
.BI \-i " image\-name" " | \-\-image=" image\-name
Container image to use as base image.
The default is derived from the
.B \-\-vendor
and
.B \-\-release
by assuming that image names follow the same pattern as the semi-official
Debian Docker images (\fBdebian:\fIRELEASE\fR) and the official
Ubuntu Docker images (\fBubuntu:\fIRELEASE\fR).
If the
.B \-\-tarball
option is also used,
this option still affects the default
.B \-\-tag
but no base image is downloaded.
Otherwise,
the image will be downloaded from a third-party container registry if required,
using the same configuration and aliasing rules that are normally used by
.BR docker-pull (1)
or
.BR podman-pull (1).
For both Debian and Ubuntu,
this will usually result in downloading
.BI docker.io/library/ VENDOR : RELEASE
from Docker Hub.

.TP
.BR "\-\-vendor debian" | "ubuntu" |...
Operating system vendor to use.
This is used to set defaults for parameters like
.B \-\-image
and
.BR \-\-mirror .
The default is to guess from the
.B \-\-release
if specified, or from the
.B \-\-mirror
if specified, or use the
.B \-\-image
as-is if specified, falling back to Debian if no image is specified.

.TP
.BI \-r " release\-name" " | \-\-release=" release\-name
Operating system release (suite) to use.
The default is to guess from the
.B \-\-image
if specified, or use
.B unstable
for Debian, or use whatever image is tagged as
.B latest
for other vendors.

.TP
.BI \-t " name " " | \-\-tag=" name
Image name to tag the new image.
When using a base image,
the default is to prepend
.B autopkgtest/
to the name of the base image, resulting in an image name like
.BR autopkgtest/debian:unstable .
When using a
.BR \-\-tarball ,
the default is to use a name based on the
.B \-\-vendor
and
.B \-\-release
(matching the default base image),
again resulting in a name like
.BR autopkgtest/debian:unstable .
If the base image starts with
.BR localhost/ ,
that prefix is removed, to reduce repetition when using podman.
If the
.B \-\-init
option is used,
then the init system is also added to the default tag name,
resulting in an image name like
.BR autopkgtest/systemd/debian:unstable .
When using Podman, if no destination registry is specified, local storage
will be assumed, resulting in a name like
.BR localhost/autopkgtest/debian:unstable .

.TP
.BR \-\-init=systemd | sysv\-rc | openrc
Add the specified init system to the container image, so that it can be
booted as a full-system container using a command like
\fBpodman run \fIIMAGE\fB /sbin/init\fR or
\fBautopkgtest\-virt\-podman \-\-init \fIIMAGE\fR.
By default, whatever init system is in the base image is kept, usually
meaning no init system at all (so the container can be used to run
individual shell commands, but cannot be booted like a full system
and will not provide the
.B isolation\-container
autopkgtest capability).
The presence or absence of an init system is recorded in the image's
metadata so that
.BR autopkgtest-virt-podman (1)
can automatically detect whether to enable or disable
.BR --init .

.TP
.BI \-m " URL" " | \-\-mirror=" URL
Mirror for apt.
The default is
.B http://deb.debian.org/debian/
for Debian, or
.B http://archive.ubuntu.com/ubuntu/
for Ubuntu, or whatever mirror is configured in the base image otherwise.

.TP
.BI \-p " URL" " | \-\-apt\-proxy=" URL
Proxy for apt. If you have an apt proxy configured on the local host, the image
will automatically use this; otherwise there is no default.

.TP
.BI "\-\-post\-command=" COMMAND
Run shell command in the image after the setup.

.TP
\fB\-\-tarball=\fIPATH\fR | \fB\-\-tarball=\-\fR
Import a pre-prepared root filesystem tarball as generated by
.BR debootstrap (1),
.BR mmdebstrap (1)
or a similar tool, and use it as the root filesystem of the container
before adding autopkgtest customizations.
If this option is used,
no base image is downloaded from third-party container registries;
the
.B \-\-image
is used to choose the default
.B \-\-tag
but is is otherwise ignored.
.IP
In addition to Essential packages,
.BR autopkgtest-virt-docker
currently requires at least the
.B apt
and
.B passwd
packages to be included,
so the smallest suitable tarball is the output of
.B "mmdebstrap --variant=apt --include=passwd"
.IR RELEASE .
It is more typical to use a
.B --variant=minbase
tarball,
which is also used as the basis for the
.BI docker.io/library/debian: RELEASE
images.

.SH ENVIRONMENT

If the
.B $AUTOPKGTEST_KEEP_APT_SOURCES
environment variable is set to a non-empty value,
.B autopkgtest\-build\-docker
will keep whatever
.B /etc/apt/sources.list
was inherited from the base image.

Otherwise, if the
.B $AUTOPKGTEST_APT_SOURCES_FILE
environment variable is set to a non-empty value,
for example
.BR AUTOPKGTEST_APT_SOURCES_FILE=/home/me/mydistro/sources.list ,
then
.B autopkgtest\-build\-docker
will read that file (on the host system) and use it to populate
.B /etc/apt/sources.list
in the virtual machine; or if the
.B $AUTOPKGTEST_APT_SOURCES
environment variable is set to a non-empty value, for example
.BR "AUTOPKGTEST_APT_SOURCES=$(cat /home/me/mydistro/sources.list)" ,
.B autopkgtest\-build\-docker
will write it directly into
.B /etc/apt/sources.list
in the resulting container image.

When
.B $AUTOPKGTEST_APT_SOURCES_FILE
or
.B $AUTOPKGTEST_APT_SOURCES
contain sources in the deb822 format, then sources are written to
.B /etc/apt/sources.list.d/<distro>.sources
in the container, rather than
.B /etc/apt/sources.list.

You can specify an apt proxy to configure in the container image in the
.B $AUTOPKGTEST_APT_PROXY
environment variable. If you have an apt proxy configured on the host, the
container will automatically use this, otherwise there is no default.

.SH EXAMPLES

A container image named
.BR autopkgtest/debian:sid ,
suitable for testing packages that target Debian unstable (sid), can be
based on the images available from docker.io:

.PP
$ autopkgtest\-build\-podman \-\-image debian:sid \-\-post\-command "apt\-get update && apt\-get install some\-package"

or can be built locally from a minbase tarball generated by
.BR mmdebstrap (1):

.PP
$ mmdebstrap \-\-variant=minbase sid \- | autopkgtest\-build\-podman \-\-release=sid \-\-tarball=\-

.SH SEE ALSO
\fBpodman\fR(1),
\fBdocker\fR(1),
\fBcontainers-registries.conf\fR(5),
\fBautopkgtest\-virt\-docker\fR(1),
\fBautopkgtest\fR(1),
\fB/usr/share/doc/autopkgtest/\fR

.SH AUTHORS AND COPYRIGHT
.B autopkgtest\-build\-docker
was written by Iñaki Malerba <inaki@malerba.space>

This manpage is part of autopkgtest, a tool for testing Debian binary
packages.  autopkgtest is Copyright (C) 2006-2014 Canonical Ltd and others.

See \fB/usr/share/doc/autopkgtest/CREDITS\fR for the list of
contributors and full copying conditions.
